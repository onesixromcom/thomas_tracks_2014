include <master-track.scad>

straightTrack(length=basicSectionLength*13);
curvedTrack(angle=40);

translate([0,2*totalTrackWidth,0]) straightTrack(length=basicSectionLength*5);
translate([0,-2*totalTrackWidth,0]) curvedTrack(angle=20);
