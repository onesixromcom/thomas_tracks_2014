render=true;
//render=false;
include <master-track.scad>

turnstyleCrossTrack();

module turnstyleCrossTrack(segments=12) {
    length=basicSectionLength * segments;
    turnstyleD=basicSectionLength * (segments-2);
    turnstyleOuterD=turnstyleD+2*margin;
    union() {
        // basic intersection
        intersection() {
          crossTrack(segments=12);
          translate([length/2, 0, floorThickness+margin-e])  
            cylinder($fn=turnstyleRingFn, d=turnstyleD, 
                h=totalTrackHeight-floorThickness-margin+ee);
        }
        
        translate([length/2, 0, 0]) innerTurnstyleRing(turnstyleD);
    }
    
    translate([0, 1.5*turnstyleD, 0]) union() {    
        difference() {
          crossTrack(segments=12);
          translate([length/2, 0, -e])  
            cylinder($fn=turnstyleRingFn, d=turnstyleOuterD, h=totalTrackHeight+ee);
        }                                    
        translate([length/2, 0, 0]) outerTurnstyleRing(turnstyleD, turnstyleOuterD);
    }    
}