// optimized for layer height 0.4
//render=true;
echo("render=", render);
PLA=false;

$fn=render?40:21;

firstLayerHeight = 0; //0.4; // offset to leave for better adhesion to bed
e = 0.01;
ee = e*2;
margin = 0.2;
maleDiameter = 8 - 2 * margin - (PLA?margin:margin); // 7.6?
maleSplitWidth = 1.5;
maleSplitLength = 6;
maleLength = 9;//9.3;//8.8;
maleNeckWidh = 7;// - margin;
femaleDiameter = 8 + (PLA?margin:margin);// 7.77; // 7.8?
femaleLength = 9;//8.9;
femaleNeckWidh=7.1 + 2*margin;
baseHeight = 8.6; // the level that the train will ride on
wheelGapWidth = 8 + 2 * margin;
trackWidth = 2.5; // the barriers ond the sides of the drive surface
pinWallWidth = 1.4;
outerSideWallWidth = pinWallWidth;
supportWidth = 0.9; //0.85;
floorThickness = 2.0; //2.2 ;//1.8 //if you substract this from the baseHeight it should be a multiple of your layer height.
tinySupportWidth = 0.4;//0.4;
gapBetweenTracksIncluding2Tracks = 15.5 + 2 * 1.5;
gapBetweenTracks = gapBetweenTracksIncluding2Tracks - 2 * trackWidth; // 
wheelCenterOffset = gapBetweenTracks/2 + trackWidth + wheelGapWidth/2;
totalTrackWidth = gapBetweenTracks + 4*trackWidth + 2*wheelGapWidth;
totalTrackHeight = 11.5;
trackHeight = totalTrackHeight - baseHeight;

slopeAngle=25;
mzSlopeAngle=15;
slopeWidth=totalTrackHeight*tan(slopeAngle);
totalTrackWidthWithSlopes = totalTrackWidth + 2*slopeWidth;
echo("totalTrackWidthWithSlopes", totalTrackWidthWithSlopes);

standardStraightTrackLength = 214; // 12*18=216
basicSectionLength = standardStraightTrackLength/18.0; //=11.8888 
crossSectionLength = 7;
crossSectionOffset = (basicSectionLength - crossSectionLength) / 2;
// from a cross section center it is 12 + 6 = 18 cross sections which is the radius of the standard circle..

maleNeckLength = maleLength-maleDiameter/2;
femaleNeckLength = femaleLength-femaleDiameter/2;

curveCenterR = 435/2; // as measured..
// basicSectionLength * (18+12); // this is just a guess..



//connectorLength = basicSectionLength * 2 - margin;//standardStraightTrackLength/20 - margin;
trackGrooveCount = 8;
grooveDepth = 0.3; // this should be a multiple of the layer height
rockRatio = render?3:1;

//curvedTrack(angle=15);
//translate([0,totalTrackWidth*2,0])straightTrack(segments=6);

turnstyleRingThickness=2*2*outerSideWallWidth;
turnstyleRingFn=200;


//== motorized constants ==//
mzFloorHeight=1.5;
mzBaseHeight=7.5;
mzTotalTrackHeight = 10.3;
mzTrackHeight = mzTotalTrackHeight - mzBaseHeight;
mzFemaleNeckLength = 4;
mzFemaleNeckWidth = wheelGapWidth; //8 ; margin added inside method
mzFemaleHeadFloorThickness = 2; // corelates with floorThickness
mzFemaleHeadLength = 5;
mzFemaleHeadWidth = 8.48;
mzMaleNeckLength = mzFemaleNeckLength + margin;
mzMaleNeckWidth = mzFemaleNeckWidth - 2*margin;
mzMaleLength = 8;
mzMaleHeadLength = mzMaleLength - mzMaleNeckLength;
mzMaleHeadWidth = mzFemaleHeadWidth;

module curvedTrack(angle=45, angleMargin=0.333) {
    angle = angle - angleMargin; // subtract a little margin
    totalDiameter=2*curveCenterR + totalTrackWidthWithSlopes;
    difference() {
        union(){
            translate([ee,0,0]) _positiveConnector();  
            difference() {
                translate([0,-curveCenterR,0])                    
                    rotate_extrude(angle = angle, convexity = 2, $fn=angle*10) {                        
                        translate([curveCenterR, 0]) 
                            trackCrossSectionShape();
                    }                   
                    
                // cut off rest of circle until we get a 2016 version of openscad..
                translate([-(totalDiameter), 
                        -(totalDiameter) + totalTrackWidthWithSlopes/2,-e]) 
                    cube([totalDiameter, 
                        totalDiameter,
                        totalTrackHeight+ee]);
                translate([0-sin(angle)*totalDiameter/2,
                    -(totalDiameter/2 - totalTrackWidthWithSlopes/2) -cos(angle)*totalDiameter/2 
                    ,-e])
                    rotate(-angle)        
                        cube([totalDiameter+ee, 
                            totalDiameter+ee,
                            totalTrackHeight+ee]);       
                    
            }           
            frontAndBackSupport(); // front
            
            translate([0, -curveCenterR,0]) rotate(-angle) translate([-supportWidth, curveCenterR,0]) // rotate about curve center
                frontAndBackSupport(); // back            
            
            translate([0, -curveCenterR,0]) rotate(-angle) translate([0, curveCenterR,0]) // rotate about curve center
                rotate([0,0,180]) translate([ee,0,0]) _positiveConnector();
        }
        femalePin();
        
        translate([0, -curveCenterR,0]) rotate(-angle) translate([0, curveCenterR,0]) // rotate about curve center        
            rotate([0,0,180]) femalePin();
        curvedTrackCutaway(angle=angle);
    }
}

module straightTrack(segments=18) {
    length=basicSectionLength * segments;
    marganalizedLength=length - margin;
    difference() {
        union(){
            translate([ee,0,0]) _positiveConnector();
            section(length=marganalizedLength);
            translate([marganalizedLength-e,0,0]) 
                rotate([0,0,180]) _positiveConnector();
        }
        femalePin();
        translate([marganalizedLength,0,0]) 
            rotate([0,0,180]) femalePin();
        trackCutaway(marganalizedLength);
    }
}

module curvedTrackCutaway(angle, height=baseHeight) {
    // right grooves
    translate([0, 0, height]) 
        curvedTrackCrossCutaways(angle=angle, width=wheelGapWidth, height=height,
                 centerOffset=-wheelCenterOffset
    );    
    
    // right bottom cutouts to force slicer to do better bridging
    translate([0, 0, height-floorThickness+grooveDepth]) 
        curvedTrackCrossCutaways(angle=angle, 
            width=wheelGapWidth+2*trackWidth, 
                height=height,
                 centerOffset=-wheelCenterOffset - supportWidth, cutDeep=true, bottomCuttaway=true
    );

    
    /* translate([0,-wheelCenterOffset - supportWidth/2, height-floorThickness+grooveDepth]) 
        curvedTrackCrossCutaways(angle=angle, width=wheelGapWidth, height=height,
                 centerOffset=-wheelCenterOffset
    
            trackCrossCutaways(length=length+2*e, width=wheelGapWidth+trackWidth+trackWidth/4+supportWidth, 
			height=grooveDepth, cutDeep=true, bottomCuttaway=true);*/
    
    // center cutouts to save on plastic..
    translate([0,-e, height+e])        
        curvedTrackCrossCutaways(angle=angle, width=gapBetweenTracks, height=height,
                  centerOffset=0, cutDeep=true); 
    
    // left grooves
    translate([0, 0, height]) 
        curvedTrackCrossCutaways(angle=angle, width=wheelGapWidth, height=height,
                 centerOffset=+wheelCenterOffset);    
                 
    // left bottom cutouts to force slicer to do better bridging
    translate([0, 0, height-floorThickness+grooveDepth]) 
        curvedTrackCrossCutaways(angle=angle, 
            width=wheelGapWidth+2*trackWidth, height=height,
                 centerOffset=+wheelCenterOffset+ supportWidth, cutDeep=true, bottomCuttaway=true);    
}

module trackCutaway(length=standardStraightTrackLength, baseHeight=baseHeight) {
    // right grooves
    translate([0,-wheelCenterOffset, baseHeight]) 
        trackCrossCutaways(length=length, width=wheelGapWidth, height=baseHeight);
    // right bottom cutouts to force slicer to do better bridging
    translate([0,-wheelCenterOffset - pinWallWidth/2, baseHeight-floorThickness+grooveDepth])   
        trackCrossCutaways(length=length+2*e, width=wheelGapWidth+trackWidth+trackWidth/4+pinWallWidth, 
			height=grooveDepth, cutDeep=true, bottomCuttaway=true);

    // center cutouts to save on plastic..
    translate([0,-e, baseHeight+e]) 
        trackCrossCutaways(length=length+2*e, width=gapBetweenTracks, height=baseHeight, centerOffset=0, cutDeep=true);

    // left grooves
    translate([0,+wheelCenterOffset, baseHeight]) 
        trackCrossCutaways(length=length, width=wheelGapWidth, height=baseHeight);
    // left bottom cutouts to force slicer to do better bridging
    translate([0, +wheelCenterOffset + pinWallWidth/2, height-floorThickness+grooveDepth]) 
        trackCrossCutaways(length=length+2*e, width=wheelGapWidth+trackWidth+trackWidth/4+pinWallWidth, 
			height=grooveDepth, cutDeep=true, bottomCuttaway=true);
}

module curvedTrackCrossCutaways(angle, width, height, centerOffset, cutDeep=false, bottomCuttaway=false) {    
    centerArcLength = 2 * PI * curveCenterR * angle/360.0;
    //echo("centerArcLength=", centerArcLength);
    crossSections = round((centerArcLength*1.0)/(basicSectionLength*1.0) - 1  );
    //echo("curved crossSections=", crossSections);
    translate([0, centerOffset, 0])  // should apply this when doing the curve rotation..
    union(){
        for(i=[-1:crossSections]) { 
            if (!bottomCuttaway || 1 <= i && i <= crossSections-2) {
                translate([0, -curveCenterR-centerOffset,0]) 
                    rotate(-i*angle/crossSections) 
                        translate([0, curveCenterR+centerOffset,0])
                            curvedTrackCrossCutaway(angle/crossSections, 
                width, height, cutDeep, centerOffset=centerOffset, cutGrooves=!bottomCuttaway);
            }            
        }
    }
}

module curvedTrackCrossCutaway(angle, 
width, height, cutDeep, centerOffset, cutGrooves=true) {
    //sectionCenterArcLength = 2 * PI * curveCenterR * 360.0 / (2*4*3*4);
    
    //gap=(centerArcLength/2)/(trackGrooveCount+1);
    //echo("gap=", gap);
    grooveLen=supportWidth/trackGrooveCount*2;
    betweenGrooveDepth=(cutDeep?crossSectionOffset:grooveDepth);
  
  // move everything up a bit  
  translate([0, -curveCenterR, 0]) 
           rotate(-0.145*angle) // too lazy to work this out as a function of width etc
             translate([0, curveCenterR, 0]) {
    
    intersection() {
        translate([0, -curveCenterR-centerOffset, 0]) 
           rotate(-0.65*angle) // too lazy to work this out as a function of width etc
             translate([0, curveCenterR+centerOffset, -crossSectionOffset])
               singleCrossCutaway(crossSectionOffset, width, 0, 0); 
        if (!cutDeep) {
			union() {              
           	  translate([0,0,-grooveDepth]) {
                translate([0, -curveCenterR-centerOffset, 0]) 
                  rotate(-0.65*angle) 
                    translate([0, curveCenterR+centerOffset, 0]) {
	                    singleCrossCutaway(grooveDepth, width, 0, 0); 
                        rocks(crossSectionOffset, width, 0); 
                    }
	            }
              }
        }
    }
    if (cutGrooves) {
       for(i=[1:trackGrooveCount]) { 
           
           translate([0, -curveCenterR-centerOffset,0]) 
             rotate(-0.25*i) 
               translate([0, curveCenterR+centerOffset,0]) // rotate about curve center
           
             singleCrossCutaway(grooveLen, width, 0,//crossSectionOffset + i * gap, 
           45); 
       }
    }  
    intersection() {
        translate([0, -curveCenterR-centerOffset, 0]) 
           rotate(0.2*angle) // too lazy to work this out as a function of width etc
             translate([0, curveCenterR+centerOffset, -crossSectionOffset])
               singleCrossCutaway(crossSectionOffset, width, 0, 0); 
        if (!cutDeep) {
			union() {              
           	  translate([0,0,-grooveDepth]) {
                translate([0, -curveCenterR-centerOffset, 0]) 
                  rotate(0.2*angle) 
                    translate([0, curveCenterR+centerOffset, 0]) {
	                    singleCrossCutaway(grooveDepth, width, 0, 0); 
                        rocks(crossSectionOffset, width, 0); 
                    }
	            }
              }
        }
    }
  }
} 

module trackCrossCutaways(length, width, height, cutDeep=false, bottomCuttaway=false) {    
    //crossSections = basicSections * 2 - 1;
    crossSections = round((length*1.0)/(basicSectionLength*1.0) - 1  );
    //echo("crossSections=", crossSections);
    union(){
        for(i=[-1:crossSections+1]) { 
            if (!bottomCuttaway || 1 <= i && i <= crossSections-1) {
                translate([i*basicSectionLength,0,0]) 
                  trackCrossCutaway(crossSectionLength, width-ee, height, cutDeep, cutGrooves=!bottomCuttaway);
            }            
        }
    }
}

module trackCrossCutaway(length, width, height, cutDeep, cutGrooves=true) {
    gap=length/(trackGrooveCount+1);
    grooveLen=supportWidth/trackGrooveCount*2;
    betweenGrooveDepth=(cutDeep?crossSectionOffset:grooveDepth);
    
    intersection() {
        translate([0,0,-crossSectionOffset])
            singleCrossCutaway(crossSectionOffset, width, 0 * gap, 0); 
        if (!cutDeep) {
			union() {
           	  translate([0,0,-grooveDepth]) {
	                translate([crossSectionOffset-grooveDepth,0, 0]) 
	                    singleCrossCutaway(grooveDepth, width, 0 * gap, 0); 
	                rocks(crossSectionOffset, width, 0 * gap); 
	            }
              }
        }
    }
    if (cutGrooves) {
       for(i=[1:trackGrooveCount]) { 
           singleCrossCutaway(grooveLen, width, crossSectionOffset + i * gap, 45); 
       }
    }
    intersection() {
        translate([0,0,-crossSectionOffset])
            singleCrossCutaway(crossSectionOffset, width, 
                crossSectionOffset + (trackGrooveCount+1) * gap, 0); 
        if (!cutDeep) {
		   union() {
                translate([0,0,-grooveDepth]) {
                    singleCrossCutaway(grooveDepth, width, crossSectionOffset + (trackGrooveCount+1) * gap, 0); 
                    rocks(crossSectionOffset, width, crossSectionOffset + (trackGrooveCount+1) * gap
                ); 
                }
            }
        }
    }    
}


module singleCrossCutaway(length, width, offset, angle) {
    translate([offset-sin(angle)*length-e,-width/2,-e]) {
        rotate([0,angle,0])
            cube([length+2*e, width, length+e]);
    }
}

module rocks(length, width, offset) {
    d=width/rockRatio;
    union(){
        for(i=[0:rockRatio/(rockRatio+3):rockRatio-1]) { 
            translate([offset+length/2 + rands(-grooveDepth,grooveDepth,1)[0], +width/3-i*d + rands(-d/4,d/4,1)[0], 0]) {
                rotate([0,0,round(rands(0,360,1)[0])]) {
                    difference() {
                        cylinder(d=d-grooveDepth,h=d, $fn=7);
                        cylinder(d=d-2*grooveDepth,h=d, $fn=5);
                    }
                }
            }
        }
    }
}

module section(length=standardStraightTrackLength, baseHeight=baseHeight, totalTrackHeight=totalTrackHeight, slopeAngle=slopeAngle) {
    union() {
        rotate([0,90,0])
        rotate([0,0,90])
        linear_extrude(height = length) trackCrossSectionShape(baseHeight=baseHeight, totalTrackHeight=totalTrackHeight, slopeAngle=slopeAngle);
        
        frontAndBackSupport(baseHeight=baseHeight, totalTrackHeight=totalTrackHeight, slopeAngle=slopeAngle); // front
        translate([length-supportWidth,0,0])
            frontAndBackSupport(baseHeight=baseHeight, totalTrackHeight=totalTrackHeight, slopeAngle=slopeAngle); // back 
    }
}


module trackCrossSectionShape(baseHeight=baseHeight, totalTrackHeight=totalTrackHeight, slopeAngle=slopeAngle) { 
    // train track cross section shape    
    slopeWidth=totalTrackHeight*tan(slopeAngle);
    totalTrackWidthWithSlopes = totalTrackWidth + 2*slopeWidth;
    polygon(points=[
        [-totalTrackWidthWithSlopes/2, 0], // bottom left
        [-totalTrackWidth/2, totalTrackHeight], //top left
        [-totalTrackWidth/2+trackWidth, totalTrackHeight], // left outer track top
        [-totalTrackWidth/2+trackWidth, baseHeight], // left outer track bottom
        [-gapBetweenTracksIncluding2Tracks/2, baseHeight], // left inner track bottom
        [-gapBetweenTracksIncluding2Tracks/2, totalTrackHeight], // left inner track top
        [-gapBetweenTracks/2, totalTrackHeight], // left inner track top
        [-gapBetweenTracks/2, baseHeight], // left inner track bottom

        [gapBetweenTracks/2, baseHeight], // right inner track bottom
        [gapBetweenTracks/2, totalTrackHeight], // right inner track top
        [gapBetweenTracksIncluding2Tracks/2, totalTrackHeight], // right inner track top
        [gapBetweenTracksIncluding2Tracks/2, baseHeight], // right inner track bottom
        [totalTrackWidth/2-trackWidth, baseHeight], // right outer track bottom
        [totalTrackWidth/2-trackWidth, totalTrackHeight], // right outer track top
        [totalTrackWidth/2, totalTrackHeight], // top right
        [totalTrackWidthWithSlopes/2, 0],  // bottom right

        // cut out the inside..
        [totalTrackWidthWithSlopes/2 -pinWallWidth, 0],  // bottom right inside
        [totalTrackWidthWithSlopes/2 -pinWallWidth - tan(slopeAngle) * (baseHeight-floorThickness), baseHeight-floorThickness],  // top right inside
        [gapBetweenTracks/2 + supportWidth , baseHeight-floorThickness],  // top right outside support             
        [gapBetweenTracks/2 + supportWidth, 0],  // bottom right outside support             
        [gapBetweenTracks/2, 0],  // bottom right inside support             
        [gapBetweenTracks/2, baseHeight-floorThickness],  // top right inside support             
        
        [-gapBetweenTracks/2, baseHeight-floorThickness],  // top left inside support
        [-gapBetweenTracks/2, 0],  // bottom left inside support
        [-gapBetweenTracks/2 - supportWidth, 0],  // bottom left outside support
        [-gapBetweenTracks/2 - supportWidth, baseHeight-floorThickness],  // top left inside support
        [-totalTrackWidthWithSlopes/2 + pinWallWidth + tan(slopeAngle) * (baseHeight-floorThickness), baseHeight-floorThickness],  // top left inside
        [-totalTrackWidthWithSlopes/2 + pinWallWidth, 0]  // bottom left inside
     ]);
}

module frontAndBackSupport(baseHeight=baseHeight, totalTrackHeight=totalTrackHeight, slopeAngle=slopeAngle) {    
    slopeWidth=totalTrackHeight*tan(slopeAngle);
    totalTrackWidthWithSlopes = totalTrackWidth + 2*slopeWidth;
    rotate([0,90,0])
    rotate([0,0,90])
    linear_extrude(height = supportWidth) {
        // train track cross section shape
        polygon(points=[
            [-totalTrackWidthWithSlopes/2, 0], // bottom left
            [-totalTrackWidth/2, totalTrackHeight], //top left
            [-totalTrackWidth/2+trackWidth, totalTrackHeight], // left outer track top
            [-totalTrackWidth/2+trackWidth, baseHeight], // left outer track bottom
            [totalTrackWidth/2-trackWidth, baseHeight], // right outer track bottom
            [totalTrackWidth/2-trackWidth, totalTrackHeight], // right outer track top
            [totalTrackWidth/2, totalTrackHeight], // top right
            [totalTrackWidthWithSlopes/2, 0],  // bottom right
         ]);
    }
}


module _positiveConnector() {
    union(){
        malePin();    
        femalePin(negative=false);            
    }
}

module femalePin(negative=true) {
    offset=negative?0:pinWallWidth;
    e=negative?e*2:0;
    translate([0,wheelCenterOffset,-e])
        union() {
            // head
            translate([femaleLength-femaleDiameter/2,0, (negative?-2*ee:0)]) 
                cylinder(d=femaleDiameter+2*offset, h=baseHeight+(negative?8*ee:0));
            // neck  (filling gaps between support)
            translate([(negative?-2*ee:0),-femaleNeckWidh/2-offset /*- (negative?0:supportWidth/2)*/,
                (negative?-e:0) + (negative?firstLayerHeight:0) ]) 
                cube([femaleNeckLength, femaleNeckWidh +2*offset /*+ (negative?0:supportWidth)*/, baseHeight+(negative?4*ee:0)]);
            
            
            if(negative) {
                    translate([-supportWidth-e,-femaleNeckWidh/2,-ee + firstLayerHeight]) 
                        cube([supportWidth, femaleNeckWidh, baseHeight+4*e]);
            }
        }
}

module malePin() {
    neckCutoutWidth = maleNeckWidh-pinWallWidth;
    supportRatio=2/3;
    inverseSupportRatio=1-supportRatio;
    translate([-maleLength,-wheelCenterOffset,0])
        difference() {
            union() {
                translate([maleDiameter/2,0,0]) cylinder(d=maleDiameter, h=baseHeight);
                translate([maleLength-maleNeckLength-e,-maleNeckWidh/2,0]) 
                    cube([maleNeckLength, maleNeckWidh, baseHeight]);
            }
            
            // hollow out in a print friendly way
            translate([maleDiameter/2,0,baseHeight*supportRatio]) cylinder(d1=maleDiameter-2*pinWallWidth, d2=e, 
                h=baseHeight*inverseSupportRatio, $fn=16);
            translate([maleDiameter/2,0,-e]) cylinder(d=maleDiameter-2*pinWallWidth, h=baseHeight*supportRatio+ee, $fn=16);
            
           translate([maleLength-maleNeckLength-e, -maleNeckWidh/2 ,-e])  
                            rotate([90,0,90]) linear_extrude(height = maleNeckLength) 
                                polygon(points=[[pinWallWidth,0],
                                    [pinWallWidth,(baseHeight-pinWallWidth)*supportRatio],
                                    [maleNeckWidh/2,baseHeight-pinWallWidth],
                                    [neckCutoutWidth,(baseHeight-pinWallWidth)*supportRatio],
                                    [neckCutoutWidth,0]]);     
            
            // split
            translate([-e,-maleSplitWidth/2,-e + firstLayerHeight]) 
                cube([maleSplitLength+e,maleSplitWidth,baseHeight+2*e]);            
        }
}

module _mzPositiveConnector() {
    union(){
        mzMalePin();    
        mzFemalePin(negative=false);            
    }
}

module mzFemalePin(negative=true) {
    offset=negative?0:pinWallWidth;
    e=negative?e*2:0;
    #translate([0,-wheelCenterOffset,-e])
        union() {
            // head
            translate([mzFemaleNeckLength+(negative?+10*ee:0), 
                -mzFemaleHeadWidth/2 -3*margin -offset,
                (negative?0:0) + (negative?firstLayerHeight:0) ])
                cube([mzFemaleHeadLength, mzFemaleHeadWidth+3*margin +2*offset , 
                    mzBaseHeight - firstLayerHeight -mzFemaleHeadFloorThickness] +2*margin);
            // neck  (filling gaps between support)            
            translate([(negative?-10*ee:0), -mzFemaleNeckWidth/2-offset,
                (negative?mzFloorHeight-margin:0) + (negative?firstLayerHeight:0) ]) 
                cube([mzFemaleNeckLength, mzFemaleNeckWidth +2*offset , mzBaseHeight+2*margin+(negative?4*ee:0)]);
            
            // extra cutaway for bridge connectors
            #translate([(negative?-10*ee:0), wheelCenterOffset - gapBetweenTracks/2 -margin -offset,                                (negative?mzFloorHeight-margin:0) + (negative?firstLayerHeight:0) ]) 
                cube([mzFemaleNeckLength, gapBetweenTracks/2 +2*margin +2*offset, 
                    mzBaseHeight - firstLayerHeight -mzFemaleHeadFloorThickness -mzFloorHeight +2*margin
                    // if the bridge is too fragile maybe cut higher.
            ]);  
        }
}

module mzFemalePinSupport() {
    offset=0;
    e=negative?e*2:0;
    #translate([0,-wheelCenterOffset,-e])
        union() {
            // head
            translate([mzFemaleNeckLength+(negative?-10*ee:0), 0,
                (negative?0:0) + (negative?firstLayerHeight:0) ])
                cube([mzFemaleHeadLength, tinySupportWidth , 
                    mzBaseHeight - firstLayerHeight -mzFemaleHeadFloorThickness]);
        }
}

module mzMalePin() {    
    union() {
        translate([-mzMaleLength,wheelCenterOffset-margin/2,+mzFloorHeight+2*margin])
            difference() {
                union() {
                    // head
                    translate([0,-mzMaleNeckWidth/2,0])
                        cube([mzMaleHeadLength, mzMaleHeadWidth-margin, 
                            mzBaseHeight -mzFloorHeight -mzFemaleHeadFloorThickness -4*margin]);
                    
                    // neck
                    translate([mzMaleHeadLength-e,-mzMaleNeckWidth/2,0]) 
                        cube([mzMaleNeckLength, mzMaleNeckWidth-margin, mzBaseHeight-mzFloorHeight-2*margin]);
                }
                
                // split
                translate([-e,-1.5*pinWallWidth+mzMaleNeckWidth/2-maleSplitWidth/2,-e + firstLayerHeight]) 
                    cube([maleSplitLength+e,maleSplitWidth,mzBaseHeight+2*e]);            
                
                // sharpen head            
                translate([-e,+mzMaleNeckWidth/2,-e + firstLayerHeight]) 
                rotate([0,0,atan((mzMaleHeadWidth-mzMaleNeckWidth)/mzMaleHeadLength)])
                    cube([maleSplitLength+e,maleSplitWidth,mzBaseHeight+2*e]);                        
            }
            
        // add support
        translate([-mzMaleLength,wheelCenterOffset-supportWidth/2,0]) {
            translate([-2*margin,-mzMaleNeckWidth/2,0])
                cube([supportWidth, mzMaleHeadWidth+supportWidth, mzFloorHeight+2*margin+e]);
            translate([mzMaleHeadLength-pinWallWidth*0.75,-mzMaleNeckWidth/2,0])
                cube([supportWidth, mzMaleHeadWidth+supportWidth, mzFloorHeight+2*margin+e]);
            
            translate([-2*margin,-mzMaleNeckWidth/2 ,0])
                cube([mzMaleHeadLength, mzMaleHeadWidth+supportWidth, tinySupportWidth]);
        }
    }
}


module stopperTrack(segments=12) {
    length=basicSectionLength * segments;
    stopperHeight = 3.5 + 0.5 + trackHeight; 
    stopperWidth = 5; 
    stopperAngle = 12;     
    stopperOffset = crossSectionOffset;// basicSectionLength/2; //10?
   
    stopperGapBetweenTracks = 20-0.5;
    
    barY = wheelGapWidth + 2*trackWidth;
    barX = barY / 3;
    verticalBarZ = 30;
    
    stopperLength =  length - stopperOffset - barX; 
    
    marganalizedLength=length - margin;
    
    widening = (stopperGapBetweenTracks-gapBetweenTracks) /2;
    
    difference() {
        union(){
            _positiveConnector();
            section(length=marganalizedLength);
            
            // stopper bar
            translate([stopperOffset, -stopperWidth/2, baseHeight]) {
                difference() {
                   cube([stopperLength, stopperWidth, stopperHeight]);
                    translate([-e, -e, 0])
                        rotate([0, -stopperAngle, 0])
                            cube([stopperLength/2, stopperWidth+ee, stopperHeight]);
                }
            }

            widenInnerTracks(widening, stopperOffset+stopperLength, side=1);
            widenInnerTracks(widening, stopperOffset+stopperLength, side=-1);
            
            // cosmetic stopper
            translate([length - barX, totalTrackWidth/2-barY, baseHeight-e])
                cube([barX, barY, verticalBarZ]);
            translate([length - barX, -totalTrackWidth/2, baseHeight-e])
                cube([barX, barY, verticalBarZ]);
            translate([length - barX - barX/2 + e, totalTrackWidthWithSlopes/2, baseHeight + verticalBarZ - barY - barX/2])
                rotate([90,0,0]) cube([barX/2, barY, totalTrackWidthWithSlopes]);
        }
        femalePin();
        trackCutaway(marganalizedLength);
    }
}

module widenInnerTracks(widening, length, side=1) {
    difference() {
        translate([0, side * gapBetweenTracksIncluding2Tracks/2 - widening/2 , baseHeight])
            cube([length, widening, trackHeight]);
        
        
        translate([-e, side * gapBetweenTracksIncluding2Tracks/2 + side * widening/2 -widening - e, baseHeight])
        rotate([0, 0, side * 7])    
                cube([length/3, widening*2+ee, trackHeight]);
    }          
}

module crossTrack(segments=12) {
    length=basicSectionLength * segments;
    marganalizedLength=length - margin;
    trackHeight = totalTrackHeight-baseHeight;    
    difference() {
        union() {
            straightTrack(segments=segments);
            
            translate([length/2, -length/2, 0])
                rotate([0, 0, 90])
                    straightTrack(segments=segments);
        }
        cutoutOfsset = gapBetweenTracks/2 + trackWidth; 
        
        translate([0, -cutoutOfsset-wheelGapWidth, baseHeight])
            cube([length, wheelGapWidth, trackHeight+e]);
        translate([0, cutoutOfsset, baseHeight])
            cube([length, wheelGapWidth, trackHeight+e]);

        translate([length/2 + cutoutOfsset, -length/2, baseHeight])
            cube([wheelGapWidth, length, trackHeight+e]);
        translate([length/2 - cutoutOfsset - wheelGapWidth, -length/2, baseHeight])
            cube([wheelGapWidth, length, trackHeight+e]);
    }    
}

module innerTurnstyleRing(turnstyleD) {
    difference() {
        translate([0, 0, floorThickness+margin])  
            cylinder($fn=turnstyleRingFn, d=turnstyleD, 
            h=baseHeight-floorThickness-margin);
        translate([0, 0, floorThickness])  
            cylinder($fn=turnstyleRingFn, d=turnstyleD-turnstyleRingThickness, 
                h=baseHeight +floorThickness+margin+e);
    }    
}

module outerTurnstyleRing(turnstyleD, turnstyleOuterD) {     
    difference() {
        cylinder($fn=turnstyleRingFn, d=turnstyleD+turnstyleRingThickness, 
            h=baseHeight);
    
        translate([0, 0, floorThickness-e])  
            cylinder($fn=turnstyleRingFn, d=turnstyleOuterD, 
                h=baseHeight+ee);
        
        translate([0, 0, -e ])  
            cylinder($fn=turnstyleRingFn, d=turnstyleD-turnstyleRingThickness, 
                h=baseHeight +floorThickness+margin+e);
    }
}

module mzConverter(segments=2) {
    length=basicSectionLength * segments;
    marganalizedLength=length - margin;
    halfCrossSectionGapLength = (basicSectionLength-crossSectionLength) / 2;
    lowLength=femaleLength+supportWidth;
    highLength=2*basicSectionLength - lowLength;
    cutoutOfsset = gapBetweenTracks/2 + trackWidth; 
    transisionLength = highLength - lowLength;
    rampAngle = atan((baseHeight-mzBaseHeight)/transisionLength);
    union() {
        difference() {
            union(){
                translate([ee,0,0]) _positiveConnector();
                section(length=highLength, baseHeight=baseHeight, slopeAngle=mzSlopeAngle);            
                translate([highLength-ee,0,0])
                    section(length=lowLength, baseHeight=mzBaseHeight, totalTrackHeight=mzTotalTrackHeight, slopeAngle=mzSlopeAngle);                        
                translate([marganalizedLength-e,0,0]) 
                    rotate([0,0,180]) _mzPositiveConnector();         
            }
            femalePin();
            translate([marganalizedLength,0,0]) 
                rotate([0,0,180]) mzFemalePin();
            trackCutaway(highLength);
            translate([highLength,0,0])
                trackCutaway(lowLength, baseHeight=mzBaseHeight);
            
            // cut ramp        
            translate([lowLength+transisionLength, -cutoutOfsset, mzBaseHeight])
                rotate([0, -rampAngle, 180])
                    cube([transisionLength*2, wheelGapWidth, trackHeight+e]);        
            translate([lowLength+transisionLength, cutoutOfsset+wheelGapWidth, mzBaseHeight])
                rotate([0, -rampAngle, 180])
                    cube([transisionLength*2, wheelGapWidth, trackHeight+e]);
            translate([lowLength+transisionLength, totalTrackWidthWithSlopes/2, mzTotalTrackHeight])
                rotate([0, -rampAngle, 180])
                    cube([transisionLength*2, totalTrackWidthWithSlopes, trackHeight+e]);
        }
        translate([marganalizedLength,0,0]) 
                rotate([0,0,180]) mzFemalePinSupport();
    }
}

