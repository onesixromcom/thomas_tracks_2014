render=true;
//render=false;
include <master-track.scad>

turnstyleSwitchCurvedTrack();

module turnstyleSwitchCurvedTrack(angle=30) {
    arcLength = 2 * PI * curveCenterR * angle/360.0;
    length = arcLength;
    trackChord=tan(angle) * curveCenterR;
    
    trackEndYOffset=curveCenterR - (cos(angle) * curveCenterR);
    turnstyleD=length - 2*basicSectionLength;    
    turnstyleOuterD=turnstyleD+2*margin;
    
    union() {
        // basic y intersection
        intersection() {
          curvedTrack(angle=angle);          
          
          translate([length/2, 0,floorThickness+margin-e])  
            cylinder($fn=turnstyleRingFn, d=turnstyleD, 
                h=totalTrackHeight-floorThickness-margin+ee);
        }
        
        translate([length/2,0,0]) innerTurnstyleRing(turnstyleD);
    }
    
    translate([0, 1.5*turnstyleD, 0]) union() {    
        difference() {
          union() {
              curvedTrack(angle=angle, angleMargin=0);
              // did some corrections below, seems the curved tracks are a bit short
              translate([length/2 -margin*3, trackEndYOffset/4    , 0])
                  rotate([0,0,180+angle])
                      translate([-length/2 +margin*4, trackEndYOffset/4, 0]) 
                          curvedTrack(angle=angle, angleMargin=0);
          }
          translate([length/2, 0,-e])  
            cylinder($fn=turnstyleRingFn, d=turnstyleOuterD, h=totalTrackHeight+ee);
        }
        
        translate([length/2, 0, 0]) 
            outerTurnstyleRing(turnstyleD,turnstyleOuterD);        
    }   
}